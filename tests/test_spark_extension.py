# NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
#
# NLDSL is licensed under a
# Creative Commons Attribution-NonCommercial 3.0 Unported License.
#
# You should have received a copy of the license along with this
# work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

import unittest
from nldsl import SparkCodeGenerator
from nldsl.core import DSLFunctionError




class TestSparkCodeGeneratorRules(unittest.TestCase):
    def setUp(self):
        self.code_gen = SparkCodeGenerator()


    def test_postprocessing(self):
        code_gen = SparkCodeGenerator(start_session_named="TestSess", stop_session=True)
        model = "## x = on df | drop duplicates"
        target_code = ["spark = SparkSession.builder.appName('TestSess').getOrCreate()",
                       "x = df.dropDuplicates()", "spark.stop()"]
        self.assertEqual(code_gen(model), target_code)

    def test_on_df(self):
        cg = SparkCodeGenerator()
        self.assertEqual(cg("## on df")[0], "df")

    def test_expr_only(self):
        cg = SparkCodeGenerator()
        input = "## z = (x > 5) and y != 7"
        self.assertEqual(cg(input)[0], "z = (x > 5) and y != 7")

    def test_create_dataframe(self):
        model = "## x = create dataframe from my_data with header 'col1', 'col2', 'col3'"
        target = "x = spark.createDataFrame(my_data, schema=['col1', 'col2', 'col3'])"
        self.assertEqual(self.code_gen(model)[0], target)

    def test_load_from(self):
        model = "## x = load from 'my_file.csv' as csv"
        target_code = "x = spark.read.format('csv').load('my_file.csv')"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_save_to(self):
        model = "## on df | save to 'filename.json' as json"
        target_code = "df.write.format('json').save('filename.json')"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_union(self):
        model = "## x = on df | union df2"
        target_code = "x = df.unionByName(df2)"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_difference(self):
        model = "## on df | difference df2"
        target_code = "df.subtract(df2)"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_intersection(self):
        model = "## on df | intersection df2"
        target_code = "df.intersect(df2)"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_select_columns(self):
        model = "## x = on df | select columns df.col1, df.col2, df.col3"
        target_code = "x = df.select([df.col1, df.col2, df.col3])"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_select_rows(self):
        model = "## x = on y | select rows y.col1 == m and y.col3 in [v1, v2, v3]"
        target_code = "x = y.filter(y.col1 == m & y.col3.isin([v1, v2, v3]))"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_drop_columns(self):
        model = "## x = on df | drop columns df.col1, 'col2', 'col3'"
        target_code = "x = df.drop([df.col1, 'col2', 'col3'])"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_join(self):
        model = "## on df | join inner df2 on 'col1', 'col2'"
        target_code = "df.join(df2, on=['col1', 'col2'], how='inner')"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_group_by(self):
        model = "## x = on df | group by 'col1' apply max"
        target_code = "x = df.groupBy(['col1']).max()"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_replace_values(self):
        model = "## x = on df | replace 5 with 'five'"
        target_code = "x = df.replace(5, 'five')"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_append_column(self):
        model = "## x = on df | append column df.col1 / 2 as 'my_col'"
        target_code = "x = df.withColumn('my_col', df.col1 / 2)"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_sort_by(self):
        model = "## on df | sort by 'col1' ascending, 'col2' descending"
        target_code = "df.sort(['col1', 'col2'], ascending=[True, False])"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_rename_columns(self):
        model = "## x = on df | rename columns 'old1' to 'new1', 'old2' to 'new2'"
        target_code = "x = df.withColumnRenamed('old1', 'new1').withColumnRenamed('old2', 'new2')"
        target_code_2 = "x = df.withColumnRenamed('old2', 'new2').withColumnRenamed('old1', 'new1')"
        result = self.code_gen(model)[0]
        self.assertTrue(result in (target_code, target_code_2))

    def test_drop_duplicates(self):
        model = "## x = on df | drop duplicates"
        target_code = "x = df.dropDuplicates()"
        self.assertEqual(self.code_gen(model)[0], target_code)

    def test_show(self):
        model = "## on df | show"
        target_code = "df.show()"
        self.assertEqual(self.code_gen(model)[0], target_code)

    def test_show_schema(self):
        model = "## on df | show schema"
        self.assertEqual(self.code_gen(model)[0], "df.printSchema()")

    def test_describe(self):
        model = "## on df | describe"
        target_code = "df.describe()"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_head(self):
        model = "## on df | head 12"
        target_code = "df.head(12)"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_count(self):
        model = "## on df | count"
        target_code = "df.count()"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_null_defect(self):
        def_model = "#$ only pos defect $col = drop duplicates | select rows $col > 0"
        eva_model = "## N = on df | only pos defect df.colA | count"
        target = "N = df.dropDuplicates().filter(df.colA > 0).count()"
        self.assertEqual(self.code_gen(def_model + eva_model)[0], target)

    def test_null_defect_2(self):
        model = "## on df | select rows df.col1 > 0"
        target = "df.filter(df.col1 > 0)"
        self.assertEqual(self.code_gen(model)[0], target)

    def test_null_defect_3(self):
        model = "## on df | select columns 0"
        target = "df.select([0])"
        self.assertEqual(self.code_gen(model)[0], target)

    def test_raises(self):
        code_gen = SparkCodeGenerator(recommend=False)
        self.assertRaises(DSLFunctionError, code_gen.__call__, "## on df | select columns")




class TestSparkCodeGeneratorExtractEnv(unittest.TestCase):
    def setUp(self):
        self.code_gen = SparkCodeGenerator()

    def test_extract_env(self):
        source_lines = [
            "my_spark = SparkSession.builder.config('x', 'y').getOrCreate()",
            "",
            "y = 5 + 7",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["spark_name"], "spark")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["spark_name"], "my_spark")

    def test_extract_env_ws(self):
        source_lines = [
            "      my_spark    =      SparkSession.builder.config('x', 'y').getOrCreate()",
            "  import   pandas  as  pd  ",
            "",
            "y = 5 + 7",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["spark_name"], "spark")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["spark_name"], "my_spark")

    def test_extract_env_no_import(self):
        source_lines = [
            "",
            "y = 5 + 7",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["spark_name"], "spark")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["spark_name"], "spark")

    def test_extract_env_no_rename(self):
        source_lines = [
            "spark = SparkSession.builder.config('x', 'y').getOrCreate()",
            "",
            "y = 5 + 7",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["spark_name"], "spark")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["spark_name"], "spark")

    def test_extract_env_multi_import(self):
        source_lines = [
            "my_spark_1 = SparkSession.builder.config('x', 'y').getOrCreate()",
            "my_spark_2 = SparkSession.builder.config('x', 'y').getOrCreate()",
            "y = 5 + 7",
            "my_spark_3 = SparkSession.builder.config('x', 'y').getOrCreate()",
            "#my_spark_4 = SparkSession.builder.config('x', 'y').getOrCreate()",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["spark_name"], "spark")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["spark_name"], "my_spark_3")

    def test_extract_env_empty_source(self):
        source_lines = []
        self.assertEqual(self.code_gen.env["spark_name"], "spark")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["spark_name"], "spark")




if __name__ == "__main__":
    unittest.main(verbosity=2)
