# NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
#
# NLDSL is licensed under a
# Creative Commons Attribution-NonCommercial 3.0 Unported License.
#
# You should have received a copy of the license along with this
# work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

import unittest
from nldsl.core import CodeMap
from nldsl import CodeGenerator, grammar




class TestDetermineInheritance(unittest.TestCase):
    def test_code_map(self):
        self.assertEqual(CodeMap._determine_inheritance(), set())
        self.assertEqual(CodeMap()._determine_inheritance(), set())


    def test_code_generator(self):
        self.assertEqual(CodeGenerator._determine_inheritance(), set([CodeMap]))
        self.assertEqual(CodeGenerator()._determine_inheritance(), set([CodeMap]))


    def test_single_inheritance(self):
        class A(CodeGenerator): pass

        res = set([CodeMap, CodeGenerator])
        self.assertEqual(A._determine_inheritance(), res)
        self.assertEqual(A()._determine_inheritance(), res)


    def test_double_inheritance(self):
        class A(CodeGenerator): pass
        class B(A): pass

        resA = set([CodeMap, CodeGenerator])
        self.assertEqual(A._determine_inheritance(), resA)
        self.assertEqual(A()._determine_inheritance(), resA)

        resB = set([CodeMap, CodeGenerator, A])
        self.assertEqual(B._determine_inheritance(), resB)
        self.assertEqual(B()._determine_inheritance(), resB)


    def test_triple_inheritance(self):
        class A(CodeGenerator): pass
        class B(A): pass
        class C(B): pass

        resA = set([CodeMap, CodeGenerator])
        self.assertEqual(A._determine_inheritance(), resA)
        self.assertEqual(A()._determine_inheritance(), resA)

        resB = set([CodeMap, CodeGenerator, A])
        self.assertEqual(B._determine_inheritance(), resB)
        self.assertEqual(B()._determine_inheritance(), resB)

        resC = set([CodeMap, CodeGenerator, A, B])
        self.assertEqual(C._determine_inheritance(), resC)
        self.assertEqual(C()._determine_inheritance(), resC)


    def test_multi_inheritance(self):
        class A(CodeGenerator): pass
        class B(A): pass
        class C(A): pass
        class D(B, C): pass

        resA = set([CodeMap, CodeGenerator])
        self.assertEqual(A._determine_inheritance(), resA)
        self.assertEqual(A()._determine_inheritance(), resA)

        resB = set([CodeMap, CodeGenerator, A])
        self.assertEqual(B._determine_inheritance(), resB)
        self.assertEqual(B()._determine_inheritance(), resB)

        resC = resB
        self.assertEqual(C._determine_inheritance(), resC)
        self.assertEqual(C()._determine_inheritance(), resC)

        resD = set([CodeMap, CodeGenerator, A, B, C])
        self.assertEqual(D._determine_inheritance(), resD)
        self.assertEqual(D()._determine_inheritance(), resD)




class TestSimpleInheritance(unittest.TestCase):
    def setUp(self):
        self.start_len = len(CodeGenerator())
        self.start_keys = list(CodeGenerator())


    def test_static_longest_name(self):
        start_sln = CodeGenerator._compute_static_longest_name()
        letter_list = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"]
        
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, " ".join(letter_list))

        self.assertEqual(A._compute_static_longest_name(), len(letter_list))
        self.assertNotEqual(A._compute_static_longest_name(), start_sln)
        self.assertEqual(CodeGenerator._compute_static_longest_name(), start_sln)


    def test_len(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(A): pass

        self.assertEqual(len(A()), 1 + self.start_len)
        self.assertEqual(len(B()), 1 + self.start_len)


    def test_keys(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(A): pass

        self.assertEqual(set(list(A().keys())), set(["fa"] + self.start_keys))
        self.assertEqual(set(list(B().keys())), set(["fa"] + self.start_keys))


    def test_getitem(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(A): pass

        self.assertEqual(A()["fa"]("_", [], {}), B()["fa"]("_", [], {}))




class TestMultiInheritance(unittest.TestCase):
    def setUp(self):
        self.start_len = len(CodeGenerator())
        self.start_keys = list(CodeGenerator())


    def test_len(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(CodeGenerator): pass
        fb = lambda code, args: code + "b"
        B.register_function(fb, "fb")

        class C(A, B): pass

        self.assertEqual(len(A()), 1 + self.start_len)
        self.assertEqual(len(B()), 1 + self.start_len)
        self.assertEqual(len(C()), 2 + self.start_len)


    def test_keys(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(CodeGenerator): pass
        fb = lambda code, args: code + "b"
        B.register_function(fb, "fb")

        class C(A, B): pass

        self.assertEqual(set(list(A().keys())), set(["fa"] + self.start_keys))
        self.assertEqual(set(list(B().keys())), set(["fb"] + self.start_keys))
        self.assertEqual(set(list(C().keys())), set(["fa"] + ["fb"] + self.start_keys))


    def test_function(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(CodeGenerator): pass
        fb = lambda code, args: code + "b"
        B.register_function(fb, "fb")

        class C(A, B): pass

        self.assertEqual(A()["fa"]("_", [], {}), C()["fa"]("_", [], {}))
        self.assertEqual(B()["fb"]("_", [], {}), C()["fb"]("_", [], {}))


    def test_getitem(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(CodeGenerator): pass
        fb = lambda code, args: code + "b"
        B.register_function(fb, "fb")

        class C(A, B): pass

        self.assertEqual(A()["fa"]("_", [], {}), C()["fa"]("_", [], {}))
        self.assertEqual(B()["fb"]("_", [], {}), C()["fb"]("_", [], {}))




class TestInstanceInheritance(unittest.TestCase):
    def setUp(self):
        self.start_len = len(CodeGenerator())
        self.start_keys = list(CodeGenerator())


    def test_len(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(A): pass
        
        a, b = A(), B()

        a["fa_instance"] = lambda code, args: code + "a_instance"
        self.assertEqual(len(a), 2 + self.start_len)
        self.assertEqual(len(b), 1 + self.start_len)

        b["fb_instance"] = lambda code, args: code + "b_instance"
        self.assertEqual(len(a), 2 + self.start_len)
        self.assertEqual(len(b), 2 + self.start_len)

        b["fb_instance_2"] = lambda code, args: code + "b_instance_2"
        self.assertEqual(len(a), 2 + self.start_len)
        self.assertEqual(len(b), 3 + self.start_len)


    def test_keys(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(A): pass
        
        a, b = A(), B()

        a["fa instance"] = lambda code, args: code + "a_instance"
        added_fun = ["fa instance"]
        self.assertEqual(set(list(a.keys())), set(added_fun + ["fa"] + self.start_keys))

        b["fb instance"] = lambda code, args: code + "b instance"
        added_fun = ["fb instance"]
        self.assertEqual(set(list(b.keys())), set(added_fun + ["fa"] + self.start_keys))

        b["fb instance two"] = lambda code, args: code + "b_instance_two"
        added_fun = ["fb instance", "fb instance two"]
        self.assertEqual(set(list(b.keys())), set(added_fun + ["fa"] + self.start_keys))


    def test_getitem(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fa")

        class B(A): pass
        
        a, b = A(), B()

        self.assertEqual(a["fa"]("_", [], {}), A()["fa"]("_", [], {}))
        self.assertEqual(b["fa"]("_", [], {}), A()["fa"]("_", [], {}))




class TestComplexInheritance(unittest.TestCase):
    def setUp(self):
        self.start_len = len(CodeGenerator())
        self.start_keys = list(CodeGenerator())


    def test_len(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fax")

        class B(CodeGenerator): pass
        fb = lambda code, args: code + "b"
        B.register_function(fb, "fbx")

        class C(CodeGenerator): pass
        fc = lambda code, args: code + "c"
        C.register_function(fc, "fcx")

        class D(A, B): pass
        fd = lambda code, args: code + "d"
        D.register_function(fd, "fdx")

        class E(A, C): pass
        fe = lambda code, args: code + "e"
        E.register_function(fe, "fex")

        class F(D, C): pass
        ff = lambda code, args: code + "f"
        F.register_function(ff, "ffx")

        class G(D, E): pass
        fg = lambda code, args: code + "g"
        G.register_function(fg, "fgx")

        self.assertEqual(len(A()), 1 + self.start_len)
        self.assertEqual(len(B()), 1 + self.start_len)
        self.assertEqual(len(C()), 1 + self.start_len)
        self.assertEqual(len(D()), 3 + self.start_len)
        self.assertEqual(len(E()), 3 + self.start_len)
        self.assertEqual(len(F()), 5 + self.start_len)
        self.assertEqual(len(G()), 6 + self.start_len)


    def test_keys(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fax")

        class B(CodeGenerator): pass
        fb = lambda code, args: code + "b"
        B.register_function(fb, "fbx")

        class C(CodeGenerator): pass
        fc = lambda code, args: code + "c"
        C.register_function(fc, "fcx")

        class D(A, B): pass
        fd = lambda code, args: code + "d"
        D.register_function(fd, "fdx")

        class E(A, C): pass
        fe = lambda code, args: code + "e"
        E.register_function(fe, "fex")

        class F(D, C): pass
        ff = lambda code, args: code + "f"
        F.register_function(ff, "ffx")

        class G(D, E): pass
        fg = lambda code, args: code + "g"
        G.register_function(fg, "fgx")

        added_fun = ["fax"]
        self.assertEqual(set(list(A().keys())), set(added_fun + self.start_keys))

        added_fun = ["fbx"]
        self.assertEqual(set(list(B().keys())), set(added_fun + self.start_keys))
        
        added_fun = ["fcx"]
        self.assertEqual(set(list(C().keys())), set(added_fun + self.start_keys))
        
        added_fun = ["fax", "fbx", "fdx"]
        self.assertEqual(set(list(D().keys())), set(added_fun + self.start_keys))
        
        added_fun = ["fax", "fcx", "fex"]
        self.assertEqual(set(list(E().keys())), set(added_fun + self.start_keys))
       
        added_fun = ["fax", "fbx", "fcx", "fdx", "ffx"]
        self.assertEqual(set(list(F().keys())), set(added_fun + self.start_keys))
        
        added_fun = ["fax", "fbx", "fcx", "fdx", "fex", "fgx"]
        self.assertEqual(set(list(G().keys())), set(added_fun + self.start_keys))


    def test_getitem(self):
        class A(CodeGenerator): pass
        fa = lambda code, args: code + "a"
        A.register_function(fa, "fax")

        class B(CodeGenerator): pass
        fb = lambda code, args: code + "b"
        B.register_function(fb, "fbx")

        class C(CodeGenerator): pass
        fc = lambda code, args: code + "c"
        C.register_function(fc, "fcx")

        class D(A, B): pass
        fd = lambda code, args: code + "d"
        D.register_function(fd, "fdx")

        class E(A, C): pass
        fe = lambda code, args: code + "e"
        E.register_function(fe, "fex")

        class F(D, C): pass

        class G(D, E): pass

        self.assertEqual(D()["fax"]("_", [], {}), A()["fax"]("_", [], {}))
        self.assertEqual(D()["fbx"]("_", [], {}), B()["fbx"]("_", [], {}))
        self.assertEqual(E()["fax"]("_", [], {}), A()["fax"]("_", [], {}))
        self.assertEqual(E()["fcx"]("_", [], {}), C()["fcx"]("_", [], {}))
        self.assertEqual(F()["fax"]("_", [], {}), A()["fax"]("_", [], {}))
        self.assertEqual(F()["fbx"]("_", [], {}), B()["fbx"]("_", [], {}))
        self.assertEqual(F()["fcx"]("_", [], {}), C()["fcx"]("_", [], {}))
        self.assertEqual(F()["fdx"]("_", [], {}), D()["fdx"]("_", [], {}))
        self.assertEqual(G()["fax"]("_", [], {}), A()["fax"]("_", [], {}))
        self.assertEqual(G()["fbx"]("_", [], {}), B()["fbx"]("_", [], {}))
        self.assertEqual(G()["fcx"]("_", [], {}), C()["fcx"]("_", [], {}))
        self.assertEqual(G()["fdx"]("_", [], {}), D()["fdx"]("_", [], {}))
        self.assertEqual(G()["fex"]("_", [], {}), E()["fex"]("_", [], {}))




if __name__ == "__main__":
    unittest.main(verbosity=2)
