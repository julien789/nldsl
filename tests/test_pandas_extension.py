# NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
#
# NLDSL is licensed under a
# Creative Commons Attribution-NonCommercial 3.0 Unported License.
#
# You should have received a copy of the license along with this
# work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

import unittest
from nldsl import PandasCodeGenerator
from nldsl.core import DSLFunctionError




class TestPandasCodeGeneratorRules(unittest.TestCase):
    def setUp(self):
        self.code_gen = PandasCodeGenerator()

    def test_on_df(self):
        self.assertEqual(self.code_gen("## on df")[0], "df")

    def test_expr_only(self):
        model = "## z = (x > 5) and y != 7"
        self.assertEqual(self.code_gen(model)[0], "z = (x > 5) and y != 7")

    def test_create_dataframe(self):
        model = "## x = create dataframe from my_data with header 'col1', 'col2', 'col3'"
        target = "x = pandas.DataFrame(my_data, columns=['col1', 'col2', 'col3'])"
        self.assertEqual(self.code_gen(model)[0], target)

    def test_load_from(self):
        model = "## x = load from 'my_file.csv' as csv"
        target_code = "x = pandas.read_csv('my_file.csv')"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_save_to(self):
        model = "## on df | save to 'filename.json' as json"
        target_code = "df.to_json('filename.json')"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_union(self):
        model = "## x = on df | union df2"
        target_code = "x = pandas.concat([df, df2])"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_difference(self):
        model = "## on df | difference df2"
        target_code = "df[~df.isin(df2).all(1)]"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_intersection(self):
        model = "## on df | intersection df2"
        target_code = "df.merge(df2)"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_select_columns(self):
        model = "## x = on df | select columns 'col1', 'col2', 'col3'"
        target_code = "x = df[['col1', 'col2', 'col3']]"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_select_rows(self):
        model = "## x = on y | select rows y.col1 == m and y.col3 in [v1, v2, v3]"
        target_code = "x = y[y.col1 == m & y.col3.isin([v1, v2, v3])]"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_drop_columns(self):
        model = "## x = on df | drop columns 'col1'"
        target_code = "x = df.drop(columns=['col1'])"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_join(self):
        model = "## on df | join inner df2 on df.col1, df.col2"
        target_code = "df.join(df2, on=[df.col1, df.col2], how='inner')"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_group_by(self):
        model = "## x = on df | group by df.col1, 'col2' apply max"
        target_code = "x = df.groupby([df.col1, 'col2']).max()"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_replace_values(self):
        model = "## x = on df | replace 5 with 'five'"
        target_code = "x = df.replace(5, 'five')"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_append_columns(self):
        model = "## x = on df | append column df.col1 / 2  as 'my_col'"
        target_inner = "df.apply(lambda row: row.col1 / 2, axis=1).values"
        target_code = "x = df.assign(**{'my_col': " + target_inner + "})"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_sort_by(self):
        model = "## on df | sort by 'col1' ascending, 'col2' descending"
        target_code = "df.sort_values(['col1', 'col2'], axis='index', ascending=[True, False])"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_drop_duplicates(self):
        model = "## x = on df | drop duplicates"
        target_code = "x = df.drop_duplicates()"
        self.assertEqual(self.code_gen(model)[0], target_code)

    def test_rename_columns(self):
        model = "## x = on df | rename columns 'old1' to 'new1', 'old2' to 'new2'"
        target_code = "x = df.rename(columns={'old1': 'new1', 'old2': 'new2'})"
        target_code_2 = "x = df.rename(columns={'old2': 'new2', 'old1': 'new1'})"
        result = self.code_gen(model)[0]
        self.assertTrue(result in (target_code, target_code_2))

    def test_show(self):
        model = "## on df | rename columns 'old1' to 'new1', 'old2' to 'new2' | show"
        target_code = "print(df.rename(columns={'old1': 'new1', 'old2': 'new2'}))"
        target_code_2 = "print(df.rename(columns={'old2': 'new2', 'old1': 'new1'}))"
        result = self.code_gen(model)[0]
        self.assertTrue(result in (target_code, target_code_2))

    def test_show_schema(self):
        model = "## on df | show schema"
        self.assertEqual(self.code_gen(model)[0], "df.info(verbose=False)")

    def test_describe(self):
        model = "## on df | describe"
        target_code = "df.describe()"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_head(self):
        model = "## on df | head 12"
        target_code = "df.head(12)"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_count(self):
        model = "## on df | count"
        target_code = "df.shape[0]"
        result = self.code_gen(model)[0]
        self.assertEqual(result, target_code)

    def test_fusion_unorderd(self):
        model_def = "#$ my pipeline $x from $y = rename columns $y to $x | drop duplicates"
        model_eva = "## x = on df | my pipeline 'new' from 'old'"
        target_code = "x = df.rename(columns={'old': 'new'}).drop_duplicates()"
        self.assertEqual(self.code_gen(model_def + model_eva)[0], target_code)

    def test_fusion_varlist(self):
        model_def = "#$ my pipeline $[$old to $new] = rename columns $[$old to $new] | show"
        model_eva = "## on df | my pipeline 'old1' to 'new1', 'old2' to 'new2'"
        target_code = "print(df.rename(columns={'old1': 'new1', 'old2': 'new2'}))"
        target_code_2 = "print(df.rename(columns={'old2': 'new2', 'old1': 'new1'}))"
        result = self.code_gen(model_def + model_eva)[0]
        self.assertTrue(result in (target_code, target_code_2))

    def test_null_defect(self):
        def_model = "#$ only pos defect $col = drop duplicates | select rows $col > 0"
        eva_model = "## N = on df | only pos defect df.colA | count"
        target = "N = df.drop_duplicates()[df.colA > 0].shape[0]"
        self.assertEqual(self.code_gen(def_model + eva_model)[0], target)

    def test_null_defect_2(self):
        model = "## on df | select rows df.col1 > 0"
        target = "df[df.col1 > 0]"
        self.assertEqual(self.code_gen(model)[0], target)

    def test_null_defect_3(self):
        model = "## on df | select columns 0"
        target = "df[[0]]"
        self.assertEqual(self.code_gen(model)[0], target)

    def test_raises(self):
        code_gen = PandasCodeGenerator(recommend=False)
        self.assertRaises(DSLFunctionError, code_gen.__call__, "## on df | select columns")




class TestPandasCodeGeneratorExtractEnv(unittest.TestCase):
    def setUp(self):
        self.code_gen = PandasCodeGenerator()

    def test_extract_env(self):
        source_lines = [
            "import pandas as pd",
            "",
            "y = 5 + 7",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["import_name"], "pandas")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["import_name"], "pd")

    def test_extract_env_ws(self):
        source_lines = [
            "  import   pandas  as  pd  ",
            "",
            "y = 5 + 7",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["import_name"], "pandas")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["import_name"], "pd")

    def test_extract_env_no_import(self):
        source_lines = [
            "",
            "y = 5 + 7",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["import_name"], "pandas")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["import_name"], "pandas")

    def test_extract_env_no_rename(self):
        source_lines = [
            "import pandas",
            "",
            "y = 5 + 7",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["import_name"], "pandas")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["import_name"], "pandas")

    def test_extract_env_multi_import(self):
        source_lines = [
            "import pandas as pd",
            "import pandas as pd2",
            "",
            "import pandas as pd3",
            "import pandas as pd4",
            "y = 5 + 7",
            "import pandas as pd5",
            "## x = load from 'my_file.csv' as csv"
        ]
        self.assertEqual(self.code_gen.env["import_name"], "pandas")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["import_name"], "pd5")

    def test_extract_env_empty_source(self):
        source_lines = []
        self.assertEqual(self.code_gen.env["import_name"], "pandas")
        self.code_gen.extract_environment(source_lines)
        self.assertEqual(self.code_gen.env["import_name"], "pandas")




if __name__ == "__main__":
    unittest.main(verbosity=2)
