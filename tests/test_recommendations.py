# NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
#
# NLDSL is licensed under a
# Creative Commons Attribution-NonCommercial 3.0 Unported License.
#
# You should have received a copy of the license along with this
# work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

import unittest
from nldsl import docs
from nldsl import PandasCodeGenerator, SparkCodeGenerator, CodeGenerator, grammar
from nldsl.core import (GrammarRule, GrammarFunction, PipeFunction, Keyword, Variable,
                        GrammarParser, KeywordRule, VariableRule, VarListRule, ExpressionRule,
                        VarList, DSLArgumentError, DSLFunctionError, Recommendation,
                        convert_function)




class TestRecommendations(unittest.TestCase):
    def test_dsl_argument_error_propagation(self):
        rule = GrammarRule([Variable("arg1"), Keyword("key"), Variable("arg2")])
        self.assertRaises(DSLArgumentError, rule.__call__, ["val1", "key"])
        self.assertRaises(ValueError, rule.__call__, ["arg1", "arg2"])


    def test_dsl_argument_error_propagation_var_list(self):
        vars = [Variable("arg1"), Keyword("key"), Variable("arg2")]
        rule = GrammarRule([VarList("var_list", vars)])
        self.assertRaises(DSLArgumentError, rule.__call__, ["val1", "key"])
        self.assertRaises(ValueError, rule.__call__, ["arg1", "arg2"])


    def test_external_dsl_function_error_propagation(self):
        def fun(code, args):
            """Desc...
            Grammar:
                dsl argument propagation test !arg1 key !arg2
            """
            return code + " " + " ".join(args)

        grammar_fun = GrammarFunction(fun)
        self.assertRaises(DSLFunctionError, grammar_fun.__call__, "", ["val1", "key"])


    def test_internal_dsl_function_error_propagation(self):
        name = "argument propagation test"
        inst = [Variable("arg1"), Keyword("key"), Variable("arg2")]
        fun_1 = lambda code, args, env: code + " ".join(args)
        fun_2 = lambda code, args, env: code + " ".join(args)
        fun_list = [(fun_1, [Variable("arg1")]), (fun_2, [Variable("arg2")])]

        pipe_fun = PipeFunction(name, inst, fun_list)
        self.assertRaises(DSLFunctionError, pipe_fun.__call__, "", ["val1", "key"])


    def test_code_gen_recommendation(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl argument propagation test !arg1 key !arg2
            """
            return code + " ".join(sorted(args.values()))

        code_gen = CodeGenerator()
        code_gen["__infer__"] = fun
        result_1 = code_gen("## dsl argument propagation test val1 key")[0]
        result_2 = code_gen("## dsl argument propagation test val1 key val2")[0]
        self.assertIsInstance(result_1, Recommendation)
        self.assertEqual(result_2, "val1 val2")


    def test_recommendations(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl argument propagation test $arg1 key !arg2
                arg1 := {min, max, avg, count}
            """
            return code + " ".join(sorted(args.values()))

        code_gen = CodeGenerator()

        code_gen["__infer__"] = fun
        result_1 = code_gen("## dsl argument propagation test")[0]
        result_2 = code_gen("## dsl argument propagation test min")[0]
        result_3 = code_gen("## dsl argument propagation test count key")[0]

        self.assertEqual(result_1.recommend(), ["min", "max", "avg", "count"])
        self.assertEqual(result_2.recommend(), ["key"])
        self.assertEqual(result_3.recommend(), [])


    def test_recommendations_incomplete_choice(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl argument propagation test $arg1 key !arg2
                arg1 := {min, max, avg, count}
            """
            return code + " ".join(sorted(args.values()))

        code_gen = CodeGenerator()

        code_gen["__infer__"] = fun
        result_1 = code_gen("## dsl argument propagation test m")[0]
        result_2 = code_gen("## dsl argument propagation test ma")[0]
        self.assertEqual(result_1.recommend(), ["min", "max"])
        self.assertEqual(result_2.recommend(), ["max"])


    def test_recommendations_var_list(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl argument propagation test $vars[$var1 $var2 key1] key2 !arg2
                var2 := {min, max, avg, count}
            """
            return code + " ".join(sorted(args.values()))

        code_gen = CodeGenerator()
        code_gen["__infer__"] = fun

        result_1 = code_gen("## dsl argument propagation test")[0]
        result_2 = code_gen("## dsl argument propagation test val1 ")[0]
        result_3 = code_gen("## dsl argument propagation test val1 min")[0]
        result_4 = code_gen("## dsl argument propagation test val1 max key1")[0]
        result_5 = code_gen("## dsl argument propagation test val1 max ke")[0]
        result_6 = code_gen("## dsl argument propagation test val1 max key1 val2")[0]
        result_7 = code_gen("## dsl argument propagation test val1 max key1 key2")[0]

        self.assertEqual(result_1.recommend(), [])
        self.assertEqual(result_2.recommend(), ["min", "max", "avg", "count"])
        self.assertEqual(result_3.recommend(), ["key1"])
        self.assertEqual(result_4.recommend(), ["key2"])
        self.assertEqual(result_5.recommend(), ["key1"])
        self.assertEqual(result_6.recommend(), ["min", "max", "avg", "count"])
        self.assertEqual(result_7.recommend(), [])


    def test_recommend_expression(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl recommend test !expression
            """
            return code + " ".join(sorted(args.values()))

        code_gen = CodeGenerator()
        code_gen["__infer__"] = fun

        result_1 = code_gen("## dsl recommend test")[0]
        result_2 = code_gen("## dsl recommend test a")[0]
        result_3 = code_gen("## dsl recommend test a < 5")[0]
        result_4 = code_gen("## dsl recommend test a <")[0]

        self.assertEqual(result_1.recommend(), [])
        self.assertEqual(result_2, "a")
        self.assertEqual(result_3, "a < 5")
        self.assertEqual(result_4.recommend(), [])


    def test_recommend_docstring(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl recommend test !expression
            """
            return code + " ".join(sorted(args.values()))

        code_gen = CodeGenerator()
        code_gen["__infer__"] = fun

        result_1 = code_gen("## dsl recommend test")[0]
        result_2 = code_gen("## dsl recommend test a <")[0]

        self.assertEqual(result_1.documentation, fun.__doc__)
        self.assertEqual(result_2.documentation, fun.__doc__)




class TestKeywordDelimiter(unittest.TestCase):
    def test_var_list_delimiter(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl varlist delimiter $vars[$var] x $var2
            """
            return code + ", ".join(args["vars"]) + ", " + args["var2"]

        code_gen = CodeGenerator()
        code_gen["__infer__"] = fun

        result_1 = code_gen("## dsl varlist delimiter")[0]
        result_2 = code_gen("## dsl varlist delimiter val1, val2")[0]
        result_3 = code_gen("## dsl varlist delimiter val1, val2 x")[0]
        result_4 = code_gen("## dsl varlist delimiter val1, val2 x val3")[0]

        self.assertEqual(result_1.recommend(), [])
        self.assertEqual(result_2.recommend(), ["x"])
        self.assertEqual(result_3.recommend(), [])
        self.assertEqual(result_4, "val1, val2, val3")


    def test_var_list_delimiter(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl varlist delimiter !expr x $var2
            """
            return code + args["expr"] + " --- " + args["var2"]

        code_gen = CodeGenerator()
        code_gen["__infer__"] = fun

        result_1 = code_gen("## dsl varlist delimiter")[0]
        result_2 = code_gen("## dsl varlist delimiter a and b")[0]
        result_3 = code_gen("## dsl varlist delimiter a x")[0]
        result_4 = code_gen("## dsl varlist delimiter z != y x 3")[0]

        self.assertEqual(result_1.recommend(), [])
        self.assertEqual(result_2.recommend(), ["x"])
        self.assertEqual(result_3.recommend(), [])
        self.assertEqual(result_4, "z != y --- 3")




class TestIncompleteExpressions(unittest.TestCase):
    def test_pandas_append_column(self):
        code_gen = PandasCodeGenerator()
        input = "## s = on x | append column as"
        self.assertEqual(code_gen(input)[0].recommend(), [])

    def test_pandas_select_rows(self):
        code_gen = PandasCodeGenerator()
        input = "## s = on x | select rows a <"
        self.assertEqual(code_gen(input)[0].recommend(), [])

    def test_pandas_select_rows_no_assign(self):
        code_gen = PandasCodeGenerator()
        input = "## on df | select rows a <"
        self.assertEqual(code_gen(input)[0].recommend(), [])

    def test_spark_append_column(self):
        code_gen = SparkCodeGenerator()
        input = "## s = on x | append column as"
        self.assertEqual(code_gen(input)[0].recommend(), [])

    def test_spark_select_rows(self):
        code_gen = SparkCodeGenerator()
        input = "## s = on x | select rows a <"
        self.assertEqual(code_gen(input)[0].recommend(), [])

    def test_spark_select_rows(self):
        code_gen = SparkCodeGenerator()
        input = "## on df | select rows a <"
        self.assertEqual(code_gen(input)[0].recommend(), [])




class TestDescription(unittest.TestCase):
    def test_keyword_desc(self):
        rule = KeywordRule("my_keyword")
        self.assertEqual(rule.description, "my_keyword")

    def test_variable_desc(self):
        rule = VariableRule("my_variable")
        self.assertEqual(rule.description, "$my_variable")

    def test_varlist_desc(self):
        vars = [VariableRule("arg1"), KeywordRule("key"), VariableRule("arg2")]
        rule = VarListRule("var_list", vars)
        self.assertEqual(rule.description, "$var_list[$arg1 key $arg2]")

    def test_expression_desc(self):
        rule = ExpressionRule("my_expr")
        self.assertEqual(rule.description, "!my_expr")

    def test_grammar_desc(self):
        rule = GrammarRule([Variable("arg1"), Keyword("key"), Variable("arg2")])
        self.assertEqual(rule.description, "$arg1 key $arg2")

    def test_grammar_varlist_desc(self):
        varlist = VarList("var_list", [Variable("var1"), Keyword("varkey"), Variable("var2")])
        rule = GrammarRule([Variable("arg1"), varlist, Keyword("key"), Variable("arg2")])
        self.assertEqual(rule.description, "$arg1 $var_list[$var1 varkey $var2] key $arg2")

    def test_recommendation_desc(self):
        rule = GrammarRule([Variable("arg1"), Keyword("key"), Variable("arg2")])
        recon = Recommendation(rule )
        self.assertEqual(recon.description, "$arg1 key $arg2")

    def test_grammar_varlist_desc(self):
        varlist = VarList("var_list", [Variable("var1"), Keyword("varkey"), Variable("var2")])
        rule = GrammarRule([Variable("arg1"), varlist, Keyword("key"), Variable("arg2")])
        recon = Recommendation(rule)
        self.assertEqual(recon.description, "$arg1 $var_list[$var1 varkey $var2] key $arg2")

    def test_grammar_parser_desc(self):
        grammar_str = "Grammar:\nmy grammar rule !expr with $my_vars[$foo $bar]"
        parser = GrammarParser(grammar_str)
        self.assertEqual(parser.grammar_rule_desc, "!expr with $my_vars[$foo $bar]")

    def test_grammar_function_desc(self):
        grammar_str = "Grammar:\nmy grammar rule !expr with $my_vars[$foo $bar]"
        parser = GrammarFunction(lambda code, args: code + " ".join(args.values()), grammar_str)
        self.assertEqual(parser.grammar_rule_desc, "!expr with $my_vars[$foo $bar]")

    def test_grammar_decorator_desc_(self):
        @grammar("Grammar:\nmy grammar rule !expr with $my_vars[$foo $bar]")
        def fun(code, args):
            return code + " ".join(args.values())

        self.assertEqual(fun.grammar_rule_desc, "!expr with $my_vars[$foo $bar]")

    def test_grammar_decorator_desc_docstring(self):
        @grammar
        def fun(code, args):
            """Grammar:
                my grammar rule !expr with $my_vars[$foo $bar]
            """
            return code + " ".join(args.values())

        self.assertEqual(fun.grammar_rule_desc, "!expr with $my_vars[$foo $bar]")

    def test_convert_function_persistence(self):
        @grammar
        def fun(code, args):
            """Grammar:
                my grammar rule !expr with $my_vars[$foo $bar]
            """
            return code + " ".join(args.values())

        converted_fun = convert_function(fun)
        self.assertEqual(converted_fun.grammar_rule_desc, "!expr with $my_vars[$foo $bar]")

    def test_convert_function_nomod(self):
        def fun(code, args):
            return code + " ".join(args.values())

        converted_fun = convert_function(fun)
        self.assertEqual(converted_fun.grammar_rule_desc, None)

    def test_attached_external_rule_persistence(self):
        @grammar
        def fun(code, args):
            """Grammar:
                my rule !expr with $my_vars[$foo $bar]
            """
            return code + " ".join(args.values())

        code_gen = CodeGenerator()
        code_gen["__infer__"] = fun
        self.assertEqual(code_gen["my rule"].grammar_rule_desc, "!expr with $my_vars[$foo $bar]")

    def test_attached_external_rule_nomod(self):
        def fun(code, args):
            return code + " ".join(args.values())

        code_gen = CodeGenerator()
        code_gen["my rule"] = fun
        self.assertEqual(code_gen["my rule"].grammar_rule_desc, None)

    def test_register_function_external_rule_persistence(self):
        @grammar
        def fun(code, args):
            """Grammar:
                my rule !expr with $my_vars[$foo $bar]
            """
            return code + " ".join(args.values())

        code_gen = CodeGenerator()
        code_gen.register_function(fun)
        self.assertEqual(code_gen["my rule"].grammar_rule_desc, "!expr with $my_vars[$foo $bar]")

    def test_register_function_external_rule_nomod(self):
        def fun(code, args):
            return code + " ".join(args.values())

        code_gen = CodeGenerator()
        code_gen.register_function(fun, "my_rule")
        self.assertEqual(code_gen["my rule"].grammar_rule_desc, None)


    def test_request_function_external_rule_persistence(self):
        @grammar
        def fun(code, args):
            """Grammar:
                my rule !expr with $my_vars[$foo $bar]
            """
            return code + " ".join(args.values())

        code_gen = CodeGenerator()
        code_gen.register_function(fun)
        self.assertEqual(code_gen.request_function("my rule").grammar_rule_desc,
                         "!expr with $my_vars[$foo $bar]")

    def test_request_function_external_rule_nomod(self):
        def fun(code, args):
            return code + " ".join(args.values())

        code_gen = CodeGenerator()
        code_gen.register_function(fun, "my_rule")
        self.assertEqual(code_gen.request_function("my rule").grammar_rule_desc, None)

    def test_internal_rule_desc(self):
        code_gen = PandasCodeGenerator()
        internal = "#$ my grammar rule $filename $[$order $col_name] = "\
                   "load from $filename as json | sort by 'data' $[$col_name $order]"
        code_gen(internal)
        self.assertEqual(code_gen["my grammar rule"].grammar_rule_desc,
                         "$filename $[$order $col_name]")




class TestFunctionInformation(unittest.TestCase):
    def test_pandas_external(self):
        fun = PandasCodeGenerator()["append column"]
        self.assertEqual(fun.__doc__, docs.APPEND_COLUMN_DOC)
        self.assertEqual(fun.grammar_rule_type, "Transformation")
        self.assertEqual("append column " + fun.grammar_rule_desc,
                         "append column !col_expr as $col_name")

    def test_pandas_internal(self):
        code_gen = PandasCodeGenerator()
        internal = "#$ select $[$col] where !cond = select columns $[$col] | select rows !cond"
        code_gen(internal)
        fun = code_gen["select"]
        self.assertEqual(fun.__doc__, "")
        self.assertEqual(fun.grammar_rule_type, "Transformation")
        self.assertEqual("select " + fun.grammar_rule_desc, "select $[$col] where !cond")

    def test_spark_external(self):
        fun = SparkCodeGenerator()["append column"]
        self.assertEqual(fun.__doc__, docs.APPEND_COLUMN_DOC)
        self.assertEqual(fun.grammar_rule_type, "Transformation")
        self.assertEqual("append column " + fun.grammar_rule_desc,
                         "append column !col_expr as $col_name")

    def test_spark_internal(self):
        code_gen = SparkCodeGenerator()
        internal = "#$ select $[$col] where !cond = select columns $[$col] | select rows !cond"
        code_gen(internal)
        fun = code_gen["select"]
        self.assertEqual(fun.__doc__, "")
        self.assertEqual(fun.grammar_rule_type, "Transformation")
        self.assertEqual("select " + fun.grammar_rule_desc, "select $[$col] where !cond")

    def test_spark_internal_2(self):
        code_gen = SparkCodeGenerator()
        internal = "#$ select $[$col] where !cond = select columns $[$col] | save to 'my_file' as json"
        code_gen(internal)
        fun = code_gen["select"]
        self.assertEqual(fun.__doc__, "")
        self.assertEqual(fun.grammar_rule_type, "Operation")
        self.assertEqual("select " + fun.grammar_rule_desc, "select $[$col] where !cond")




class TestDisableRecommendations(unittest.TestCase):
    def setUp(self):
        self.code_gen = CodeGenerator(recommend=False)


    def test_code_gen_raises(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl argument propagation test !arg1 key !arg2
            """
            return code + " ".join(sorted(args.values()))

        self.code_gen["__infer__"] = fun

        input_1 = "## dsl argument propagation test val1 key"
        input_2 = "## dsl argument propagation test val1 key val2"

        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_1)
        self.assertEqual(self.code_gen(input_2)[0], "val1 val2")


    def test_code_gen_raises_2(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl argument propagation test $arg1 key !arg2
                arg1 := {min, max, avg, count}
            """
            return code + " ".join(sorted(args.values()))

        self.code_gen["__infer__"] = fun

        input_1 = "## dsl argument propagation test"
        input_2 = "## dsl argument propagation test min"
        input_3 = "## dsl argument propagation test count key"

        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_1)
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_2)
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_3)


    def test_raises_incomplete_choice(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl argument propagation test $arg1 key !arg2
                arg1 := {min, max, avg, count}
            """
            return code + " ".join(sorted(args.values()))

        self.code_gen["__infer__"] = fun

        input_1 = "## dsl argument propagation test m"
        input_2 = "## dsl argument propagation test ma"

        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_1)
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_2)


    def test_raises_var_list(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl argument propagation test $vars[$var1 $var2 key1] key2 !arg2
                var2 := {min, max, avg, count}
            """
            return code + " ".join(sorted(args.values()))

        self.code_gen["__infer__"] = fun

        input_1 = "## dsl argument propagation test"
        input_2 = "## dsl argument propagation test val1 "
        input_3 = "## dsl argument propagation test val1 min"
        input_4 = "## dsl argument propagation test val1 max key1"
        input_5 = "## dsl argument propagation test val1 max ke"
        input_6 = "## dsl argument propagation test val1 max key1 val2"
        input_7 = "## dsl argument propagation test val1 max key1 key2"

        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_1)
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_2)
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_3)
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_4)
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_5)
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_6)
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_7)


    def test_recommend_expression(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl recommend test !expression
            """
            return code + " ".join(sorted(args.values()))

        self.code_gen["__infer__"] = fun

        input_1 = "## dsl recommend test"
        input_2 = "## dsl recommend test a"
        input_3 = "## dsl recommend test a < 5"
        input_4 = "## dsl recommend test a <"

        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_1)
        #self.assertEqual(self.code_gen(input_2)[0], "a")
        self.assertEqual(self.code_gen(input_3)[0], "a < 5")
        self.assertRaises(DSLFunctionError, self.code_gen.__call__, input_4)


    def test_recommend_docstring(self):
        @grammar
        def fun(code, args):
            """Desc...
            Grammar:
                dsl recommend test !expression
            """
            return code + " ".join(sorted(args.values()))

        self.code_gen["__infer__"] = fun

        try:
            self.code_gen("## dsl recommend test")[0]
        except DSLFunctionError as err:
            self.assertEqual(err.doc, fun.__doc__)

        try:
            self.code_gen("## dsl recommend test a <")[0]
        except DSLFunctionError as err:
            self.assertEqual(err.doc, fun.__doc__)




if __name__ == "__main__":
    unittest.main(verbosity=2)
